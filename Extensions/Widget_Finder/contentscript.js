function init() {
  let data = [];

  let all_widgets = document.querySelectorAll("[class|=js-appwiki]");

  let report = "No Appwiki widget found";
  let count = 0;

  let v3_classes = [
    "js-appwiki-filter-buttons",
    "js-appwiki-list-medium-widget",
    "js-appwiki-text-logo-widget",
    "js-appwiki-top-usps",
    "js-appwiki-list-minimal-widget",
    "js-appwiki-text-widget",
    "js-appwiki-horizontal-widget",
    "js-appwiki-target-selects",
    "js-appwiki-top-minimal",
  ];

  let checkVersion3 = (widget) => {
    for (var i = 0; i < v3_classes.length; i++) {
      if (widget.classList.contains(v3_classes[i])) {
        return true;
      }
    }
    return false;
  };

  let checkVersion2 = (widget) => {
    return widget.querySelector("[id|=appwiki-plugin]") !== null;
  };

  let checkVersion1 = (widget) => {
    return (
      widget.querySelector(".swv-wp-list") !== null ||
      widget.querySelector(".c-campaign") !== null
    );
  };

  if (all_widgets.length > 0) {
    report = `Total widgets: ${all_widgets.length}`;

    for (let i = 0; i < all_widgets.length; i++) {
      let new_Data = {};

      if (checkVersion3(all_widgets[i])) {
        if (all_widgets[i].classList.contains("js-appwiki-target-selects")) {
          let filter_category = all_widgets[i].getAttribute("data-category");
          report += `\n\n${count}. Select Category: ${filter_category} \nVERSION: V3`;
          new_Data = {
            version: "3",
            widget_key: "Select Category",
            widget_key_value: filter_category,
            index: i,
          };
        } else if (
          all_widgets[i].classList.contains("js-appwiki-filter-buttons")
        ) {
          let filter_category = all_widgets[i].getAttribute("data-category");
          report += `\n\n${count}. FILTER BUTTONS CATEGORY: ${filter_category} \nVERSION: V3`;
          new_Data = {
            version: "3",
            widget_key: "Select Category:",
            widget_key_value: filter_category,
            index: i,
          };
        } else {
          let feed_key = all_widgets[i].getAttribute("data-feed");
          report += `\n\n${count}. FEED KEY: ${feed_key} \nVERSION: V3`;
          new_Data = {
            version: "3",
            widget_key: "FEED KEY",
            widget_key_value: feed_key,
            index: i,
          };
        }
      } else if (checkVersion2(all_widgets[i])) {
        let feed_key = all_widgets[i].getAttribute("data-key");
        report += `\n\n${count}. FEED KEY: ${feed_key} \nVERSION: V2`;
        new_Data = {
          version: "2",
          widget_key: "FEED KEY",
          widget_key_value: feed_key,
          index: i,
        };
      } else if (checkVersion1(all_widgets[i])) {
        let feed_key = all_widgets[i].getAttribute("data-key");
        report += `\n\n${count}. FEED KEY: ${feed_key} \nVERSION: V1`;
        new_Data = {
          version: "1",
          widget_key: "FEED KEY",
          widget_key_value: feed_key,
          index: i,
        };
      } else {
        console.log(`Widget ${all_widgets[i].length} failed version check`);
      }

      data.push(new_Data);
    }
    console.log(report);
  }

  function find_widget(pos) {
    console.log("triggered 1");
    if (pos < all_widgets.length) {
      console.log("triggered 4");
      let widget = all_widgets[pos];
      widget.scrollIntoView({
        behavior: "smooth",
        block: "center",
        inline: "start",
      });
      if (widget.style.backgroundColor != "rgb(241, 241, 241)") {
        let previousStyling = widget.style.backgroundColor;
        widget.style.backgroundColor = "rgb(241, 241, 241)";
        setTimeout(function () {
          widget.style.backgroundColor = previousStyling;
        }, 1500);
      }
    }
  }

  chrome.runtime.onMessage.addListener(function (
    message,
    sender,
    sendResponse
  ) {
    switch (message.type) {
      case "getdata":
        sendResponse(data);
        break;
      case "findWidget":
        let pos = message.position;
        sendResponse(find_widget(pos));
        break;
      default:
        console.error("Unrecognised message: " + message);
    }
    return true;
  });
}

window.onload = function () {
  setTimeout(function () {
    init();
  }, 2000);
};
