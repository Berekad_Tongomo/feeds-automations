let status = document.querySelector("#status");
let wrapper = document.querySelector("#card-wrapper");
let feed;
let loader = document.querySelector(".loader-ripple");
let query = document.getElementById("query");
let position;

function selectWidget(card_obj) {
  feed = card_obj.querySelector(".card-title").innerText;
  position = card_obj.getAttribute("position");

  chrome.extension.getBackgroundPage().tellContentScripts(position);

  var orig = card_obj.style.color;
  card_obj.style.backgroundColor = "#00a300";
  card_obj.classList.add("copied");
  setTimeout(function () {
    card_obj.style.backgroundColor = orig;
    card_obj.classList.remove("copied");
  }, 500);
}

function copy_id(card_title_obj) {
  let widget_id = card_title_obj.innerText;

  navigator.permissions.query({ name: "clipboard-write" }).then((result) => {
    if (result.state == "granted" || result.state == "prompt") {
      navigator.clipboard.writeText(widget_id);
      console.log(`widget id ${widget_id} copied to clipboard`);
    }
  });
}

chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
  function try_connect() {
    chrome.tabs.sendMessage(tabs[0].id, { type: "getdata" }, function (data) {
      if (chrome.runtime.lastError) {
        setTimeout(try_connect, 1000);
      } else {
        let create_card = (id, version, index) => {
          let card_title = document.createElement("p");
          card_title.classList = "card-title";
          card_title.innerHTML = id;
          // clipboard here?
          let span = document.createElement("span");
          span.setAttribute("title", "Copy");
          span.classList = "clipboard";
          span.onclick = function () {
            copy_id(card_title);
          };
          let img = document.createElement("img");
          img.src = "images/clipboard.png";
          span.appendChild(img);
          card_title.appendChild(span);

          let card_subtitle = document.createElement("p");
          card_subtitle.classList = "card-subtitle";
          card_subtitle.innerHTML = `Version: ${version}`;

          let card = document.createElement("li");
          card.classList = "card";
          card.setAttribute("position", index);
          card.setAttribute("name", id);
          card.appendChild(card_title);
          card.appendChild(card_subtitle);

          card.onclick = function () {
            selectWidget(card);
          };

          wrapper.appendChild(card);
        };

        let create_cards = () => {
          for (i = 0; i < data.length; i++) {
            let card_title = document.createElement("p");
            card_title.classList = "card-title";
            card_title.innerHTML = data[i].widget_key_value;
            // clipboard here?
            let span = document.createElement("span");
            span.setAttribute("title", "Copy");
            span.classList = "clipboard";
            span.onclick = function () {
              copy_id(card_title);
            };
            let img = document.createElement("img");
            img.src = "images/clipboard.png";
            span.appendChild(img);
            card_title.appendChild(span);

            let card_subtitle = document.createElement("p");
            card_subtitle.classList = "card-subtitle";
            card_subtitle.innerHTML = `Version: ${data[i].version}`;

            let card = document.createElement("li");
            card.classList = "card";
            card.setAttribute("position", data[i].index);
            card.setAttribute("name", data[i].widget_key_value);
            card.appendChild(card_title);
            card.appendChild(card_subtitle);

            card.onclick = function () {
              selectWidget(card);
            };

            wrapper.appendChild(card);
          }
        };

        let setStatus = () => {
          if (data.length > 0) {
            status.innerText =
              data.length == 1 ? `1 widget` : `${data.length} widgets`;
            create_cards();
            query.style.visibility = "visible";
          } else {
            status.innerText = `No widgets`;
          }
          loader.style.display = "none";
        };

        let updateStatus = () => {
          if (wrapper.childElementCount > 0) {
            status.innerText =
              wrapper.childElementCount == 1
                ? `1 widget`
                : `${wrapper.childElementCount} widgets`;
          } else {
            status.innerText = `No widgets`;
          }
        };

        function updateCards(query) {
          wrapper.innerHTML = "";

          data.map(function (data_obj) {
            query.split(" ").map(function (word) {
              if (
                data_obj.widget_key_value
                  .toLowerCase()
                  .indexOf(word.toLowerCase()) != -1
              ) {
                create_card(
                  data_obj.widget_key_value,
                  data_obj.version,
                  data_obj.index
                );
              }
              updateStatus();
            });
          });
        }

        query.oninput = function () {
          updateCards(this.value);
        };

        query.addEventListener("keypress", function (e) {
          if (e.key === "Enter") {
            console.log("enter pressed");
            data.map(function (data_obj) {
              if (query.value == data_obj.widget_key_value) {
                let key = document.querySelector(
                  `[name='${data_obj.widget_key_value}']`
                );
                if (key != null) {
                  selectWidget(key);
                }
              }
            });
          }
        });

        setStatus();

        console.log(data);
      }
    });
  }

  try_connect();
});
